
README file for the Biblio Normalize Drupal module.


Description
***********

Biblio Normalize provides "normalized" data for Biblio's multiple-values fields
(such as Authors and Keywords fields), making it easier for other modules to
perform queries on those values.

This module creates a new database table where each value has its own
row. Values are synchronized each time Biblio updates a node.

This module doesn't provide anything directly useful to end-users. It is only
meant as a service to other modules. For example, Biblio Facets
(http://drupal.org/project/biblio_facets) uses Biblio Normalize.


Requirements
************

- Drupal 5.x (http://drupal.org/project/drupal).

- Biblio (http://drupal.org/project/biblio).


Installation
************

1. Extract the 'biblio_normalize' module directory into your Drupal modules
   directory.

2. Go to the Administer > Site building > Modules page, and enable Biblio
   Normalize.

3. Go to the Administer > Site configuration > Biblio settings page, and click
   the Field normalization tab. Check each multiple-values field you wish to
   normalize, and enter the delimiter that separates the values. Click the Save
   configuration button to apply the settings.

   The default delimiter for all fields is the comma (,) but for an Authors
   field, the semi-colon (;) is usually used. 


Support
*******

For support requests, bug reports, and feature requests, please use the
project's issue queue on http://drupal.org/project/issues/biblio_normalize.


Credits
*******

* Sponsored by Laboratoire NT2 (http://www.labo-nt2.uqam.ca).
